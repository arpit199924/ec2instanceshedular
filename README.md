# EC2instanceShedular
- Create a virtual environment using `virtualenv ec2` 
- activate the same virtualenvironment using `source ec2/bin/activate`
- clone this repo in parallel to this environment and go to this folder
- run `python lambdamaker.py` inside terminal, this will generate a lambda.zip file
- now go to AWS account and create an IAM role with cloudwatch : (read, write), EC2 : (start, stop), cloudwatchEvents : (read, write) permissions
- now create AWS Lambda with above role 
- Create an API Gateway service
- inside API Gateway service create a resource /ec2-scheduler
- Create GET, PUT, DELETE methods with Lambda_proxy : enabled, Integration type : Lambda function, lambda name : above lambda name
- GET method will return all the instance ids with tag scheduled : true
- PUT method will allow the scheduling of the specified instance ids acording to the given action(start, stop)
- DELETE method will delete the schedule of the specified instance id
- create some ec2 instances for testing
- Sample requests and responses are there in Postman collection named ec2schedulerforopslyft(use your base URL for testing)
- Schedule should be in GMT time zone

