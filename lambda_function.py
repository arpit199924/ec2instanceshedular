import json
import boto3
from croniter import croniter
import string
from random import choice


def lambda_handler(event, context):
    print(context.invoked_function_arn, context.function_name)
    if isinstance(event, str):
        event = json.loads(event)
    client = boto3.client('ec2')
    # To get all the scheduled instances
    if event.get('event_x') and event.get('event_x')=='cloud_watch':
        action = event.get('action')
        if action == "start":
            if client.describe_instances(InstanceIds=[event.get('instance_id')]):
                client.start_instances(InstanceIds = [event.get('instance_id')])
                response = "Successfully started your instance" + str([event.get('instance_id')])
        elif action == "stop":
            if client.describe_instances(InstanceIds=[event.get('instance_id')]):
                client.stop_instances(InstanceIds = [event.get('instance_id')])
                response = "successfully stopped your instance" + str([event.get('instance_id')])
        else:
            response = {'action':None}
    elif event.get("httpMethod") == "GET":
        filters =[
                 {'Name': 'tag:scheduled', 'Values': ["true"]},
                 ]
        output = client.describe_instances(Filters = filters)
        a = []
        for item in output.get("Reservations"):
            for jitem in item.get("Instances"):
                a.append(jitem.get("InstanceId"))
        response = {'scheduled_instances':a}
    #To update instances
    elif event.get('body') and event.get("httpMethod") == "PUT":
        body = event.get('body')
        if isinstance(event.get('body'), str):
            body = json.loads(event.get('body'))
        instance_id = body.get("instance_id")
        schedule = body.get("schedule")
        action = body.get("action")
        if instance_id and schedule and action and action in ['start','stop'] and croniter.is_valid(schedule):
            cron_ex = schedule.split(" ")
            cron_ex.insert(4,"?")
            cron_ex = f"cron({' '.join(cron_ex)})"
            cw_client = boto3.client('events')
            cw_rules = cw_client.list_rules(NamePrefix=f'{instance_id}-{action}',Limit=100)
            lclient = boto3.client('lambda')
            if cw_rules and cw_rules.get("Rules"):
                for name in cw_rules.get("Rules"):
                    res= cw_client.list_targets_by_rule(Rule=name.get('Name'))
                    try:
                        lclient.remove_permission(FunctionName=context.function_name,StatementId=f'{context.function_name}-invoke-{instance_id}-{action}')
                    except:
                        pass
                    if res.get('Targets'):
                        cw_client.remove_targets(Rule=name.get('Name'),Ids=[ id.get('Id') for id in res.get('Targets')])
                    cw_client.delete_rule(Name=name.get('Name'))
            random_str = ''.join(choice(string.ascii_lowercase + string.digits.replace('0', '')) for _ in range(4))
            resp = cw_client.put_rule(Name=f"{instance_id}-{action}-{random_str}",ScheduleExpression=cron_ex,State='ENABLED',Description=f'This rule is enabled for {instance_id}')
            lclient.add_permission(FunctionName=context.function_name,StatementId=f'{context.function_name}-invoke-{instance_id}-{action}',Action='lambda:InvokeFunction',Principal='events.amazonaws.com',SourceArn=resp.get('RuleArn'))
            cw_client.put_targets(Rule=f"{instance_id}-{action}-{random_str}",Targets=[{'Id': f"{instance_id}-target","Input":json.dumps({"instance_id":instance_id,"event_x":"cloud_watch", "action":action}),'Arn': context.invoked_function_arn,}])
            client.create_tags(Resources=[instance_id],Tags=[{'Key': 'scheduled','Value': "true"}])
            response = {'status':'schedule applied'}
        else:
            response = {'error': 'invalid params'}
    
    #To delete rules
    elif event.get("httpMethod") == "DELETE":
        body = event.get('body')
        if isinstance(event.get('body'), str):
            body = json.loads(event.get('body'))
        instance_id = body.get("instance_id")
        cw_client = boto3.client('events')
        lclient = boto3.client('lambda')
        cw_rules = cw_client.list_rules(NamePrefix=instance_id,Limit=100)
        if cw_rules and cw_rules.get("Rules"):
                for name in cw_rules.get("Rules"):
                    res= cw_client.list_targets_by_rule(Rule=name.get('Name'))
                    try:
                        lclient.remove_permission(FunctionName=context.function_name,StatementId=f'{context.function_name}-invoke-{instance_id}-stop')
                    except :
                        pass
                    try:
                        lclient.remove_permission(FunctionName=context.function_name,StatementId=f'{context.function_name}-invoke-{instance_id}-start')
                    except :
                        pass
                    if res.get('Targets'):
                        cw_client.remove_targets(Rule=name.get('Name'),Ids=[ id.get('Id') for id in res.get('Targets')])
                    cw_client.delete_rule(Name=name.get('Name'))
        client.create_tags(Resources=[instance_id],Tags=[{'Key': 'scheduled','Value': "false"}])
        response = {'status':'schedule removed'}
    return {
        'statusCode': 200,
        'body': json.dumps(response)
    }
